package project01;

public class Beverage extends Product {
    private double ph;
    private double lts;
    private double alcohol;
    private String brand;

    public Beverage(String name, double tax, double price, String refund, String brand, double ph, double lts) {
        super(name, tax, price, refund);
        this.ph = ph;
        this.lts = lts;
        this.brand = brand;

    }

    public double getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(double alcohol) {
        this.alcohol = alcohol;
    }


    public String toString() {
        if(this.alcohol>0) {
            return super.toString() + "Brand: " + this.brand + "\n" + "pH Level: " + this.ph + "\n" + "Litres: " + this.lts + "\n" +
                    "Alcohol percentage: " + this.alcohol + "\n";
        }
        else {
            return super.toString() + "Brand: " + this.brand + "\n" + "pH Level: " + this.ph + "\n" + "Litres: " + this.lts + "\n" ;
        }
    }
    public double calculateTotalPrice () {
        return (getPrice()*(getTax() + 100)/100)*lts;
    }
}
