package project01;

public class Cloth extends Product {

    private String size; //constructors also apply to the Tshirt class by super function
    private String brand;
    private Double quantity;

    public Cloth(String name, double tax, double price, String refund, String size, String brand, Double quantity) {
        super(name, tax, price, refund);
        this.size = size;
        this.brand = brand;
        this.quantity = quantity;

    }
    public Double getQuantity(){
        return this.quantity;
    }
    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
    //printing attributes

    public String toString() {
        return super.toString() +
                "Size: " + this.size + "\n" +
                "Brand: " + this.brand + "\n" +
                "Quantity: " + this.quantity ;
    }
    // calculating price
    public double calculateTotalPrice () {
        return ((getPrice())*(getTax() + 100)/100)*quantity;
    }
    ////deciding whether or not the product is shrinkable
    public void shrink(){
        System.out.println("This clothing can shrink when washed at high temperature.");
    }

}
