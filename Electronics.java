package project01;

public class Electronics extends Product{
    private String brand;  //constructors are private because they will be used only here
    private double screen;
    private double tax;

    public Electronics(String name, double tax, double price, String refund, String brand, double screen){
        super(name, tax, price, refund);
        this.brand = brand;
        this.screen = screen;

    }

//printing statements
    public String toString() {
        return super.toString() +
                "Screen Size: " + this.screen + " inches " + "\n" +
                "Brand: " + this.brand + "\n" ;

    }
// calculating price
    public double calculateTotalPrice () {
        return (getPrice()*(getTax() + 100)/100);
    }






}
