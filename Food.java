package project01;

public class Food extends Product {
    private String expirationDate;

    public Food(String name, double tax, double price, String refund, String expirationDate){
        super(name, tax, price, refund);
        this.expirationDate = expirationDate;
    }

    public double calculateTotalPrice () {
        return (getPrice()*(getTax() + 100)/100);
    }



}
