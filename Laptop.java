package project01;

public class Laptop extends Electronics {
    private String gpu;  //constructors are private because they will be used only here
    private String ram;
    private String processor;


    public Laptop(String name, double tax, double price, String refund, String brand, double screen, String gpu, String ram, String processor){
        super(name, tax, price, refund, brand, screen);
        this.gpu = gpu;
        this.ram = ram;
        this.processor = processor;

    }

    public String wrap(String x, String b){
      //  System.out.println("run "+ x);
        return "This product can be wrapped.";
    }
    public String wrap(String a){
        return "This product cannot be wrapped.";
    }

    public String toString() {
        return super.toString() + "GPU: " + this.gpu + "\n" + "RAM: " + this.ram + "\n" + "Processor: " + this.processor + "\n" + wrap("a","b") + "\n";
    }
    public double calculateTotalPrice () {
        return ((getPrice())*(getTax() + 100)/100);
    }

    }
