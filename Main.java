package project01;

import java.util.ArrayList;

public class Main {
    public static void main(String args[]) {
        // This program prints the properties of products that has been entered and then calculates the total price

        ArrayList<Product> products = new ArrayList<Product>();

        //following part is adding products to the Product arraylist

        Phone phone1 = new Phone("Phone", 20, 1000, "No", "Iphone", 5.8, "16 MP wide", "A13 Bionic", 64);
        products.add(phone1);

        Laptop laptop1 = new Laptop("Laptop", 25, 2000, "Yes", "Asus", 27, "GeForce 1660Ti", "HyperX 16 GB", "Intel");
        products.add(laptop1);

        Beverage drink1 = new Beverage("Beverage", 30, 120, "No", "Johnnie Walker", 8.7, 1.75);
        drink1.setAlcohol(40);
        products.add(drink1);

        Beverage drink2 = new Beverage("Beverage", 12, 120, "No", "Sprite", 10.2, 2);
        products.add(drink2);

        Snack snack1 = new Snack("Snack", 15, 10, "No", "10-10-2020", 5, "Nestle", 45);
        products.add(snack1);

        Sport sport1 = new Sport("Sport", 20, 20, "Yes", "Football", "Nike", 3);
        products.add(sport1);

        Videogame game1 = new Videogame("Videogame", 15, 250, "No", "PS4", "God of War");
        products.add(game1);

        Tshirt shirt1 = new Tshirt("T-Shirt", 15, 50, "No", "Medium", "Adidas", 10);
        products.add(shirt1);


        double totalPrice = 0;


        // this part calculates the total price
        for (Product product : products) {
            if (product instanceof Cloth) {
                System.out.println(product);
                shirt1.shrink();
                System.out.println();
                totalPrice += ((Cloth) product).calculateTotalPrice();
            }
        }
        for (Product product : products) {
            if (product instanceof Food) {
                System.out.println(product);
                totalPrice += ((Food) product).calculateTotalPrice();
            }
        }
        for (Product product : products) {
            if (product instanceof Videogame) {
                System.out.println(product);
                totalPrice += ((Videogame) product).calculateTotalPrice();
            }
        }
        for (Product product : products) {
            if (product instanceof Electronics) {
                System.out.println(product);
                totalPrice += ((Electronics) product).calculateTotalPrice();
            }
        }
        for (Product product : products) {
            if (product instanceof Sport) {
                System.out.println(product);
                totalPrice += ((Sport) product).calculateTotalPrice();
            }
        }
        for (Product product : products) {
            if (product instanceof Beverage) {
                System.out.println(product);
                totalPrice += ((Beverage) product).calculateTotalPrice();
            }
        }
        //printing total price
        System.out.println("Total is " + totalPrice + " TL");

    }
    // these methods are for method overriding on classes under
    public static void secondhand() {
        System.out.println("This product has never been used before.");
    }

    public static void stock() {
        System.out.println("This product is available.");
    }

}









