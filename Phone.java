package project01;

public class Phone extends Electronics {
    private String camera;  //constructors are private because they will be used only here
    private String cpu;
    private double storage;


    public Phone(String name, double tax, double price, String refund, String brand, double screen, String camera, String cpu, double storage){
        super(name, tax, price, refund, brand, screen);
        this.camera = camera;
        this.cpu = cpu;
        this.storage = storage;
    }
    public String secondhand(){
        return "This product has been used once before.";
    }


    public String toString() {
        return super.toString() + "Camera: " + this.camera + "\n" + "CPU: " + this.cpu +
                "\n" + "Storage: " + this.storage + " GB " + "\n" + secondhand() + "\n";


    }
    //calculating price
    public double calculateTotalPrice () {
        return ((getPrice())*(getTax() + 100)/100);
    }



}
