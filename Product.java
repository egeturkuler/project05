package project01;

public class Product {
    private String name; //main constructors for all products
    private double tax;
    private double price;
    private String refund;

    public Product(String name, double tax, double price, String refund){
        this.name = name;
        this.refund = refund;
        this.tax = tax;
        this.price = price;
    }
    //adding setter and getter
    public double getTax(){
        return this.tax;
    }
    public void setTax(double tax) {
        this.tax = tax;
    }
    public double getPrice(){
        return this.price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

   //printing statements
    public String toString() {
        return
                 "Product: " + this.name + "\n" + "Tax rate: " + this.tax + "\n" + "Price: "
                        + this.price + "\n" + "Refundable: " + this.refund + "\n";
    }



}
