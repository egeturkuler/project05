package project01;

public class Snack extends Food {
    private double quantity;
    private String brand;
    private double calorie;

    public Snack(String name, double tax, double price, String refund, String expirationDate, double quantity, String brand, double calorie){
        super(name,tax,price,refund,expirationDate);
        this.quantity = quantity;
        this.brand = brand;
        this.calorie = calorie;
    }
    //using method overload to determine if the product contains sugar or not
    public String diabetes(int x,String y){
        return "This snack is sugar-free." ;
    }

    public String diabetes(String x, int y){
        return "This snack has sugar in it." ;
    }

    //printing statements
    public String toString() {
        return super.toString() +
                "Brand: " + this.brand + "\n" +
                "Quantity: " + this.quantity + "\n" + "Calorie: " + this.calorie + "\n" +
                diabetes(1,"a") + "\n";

    }
    //calculating price
    public double calculateTotalPrice () {
        return (getPrice()*(getTax() + 100)/100*quantity);
    }

}
