package project01;

public class Sport extends Product {
    private String sportkind;  //constructors are private because they will be used only here
    private String brand;
    private double quantity;

    public Sport(String name, double tax, double price, String refund, String sportkind, String brand, double quantity) {
        super(name, tax, price, refund);
        this.brand = brand;
        this.quantity = quantity;
        this.sportkind = sportkind;
    }

//using method override to determine if the product is available
    public String  stock(){
        return "This product is no longer available.";
    }
// printing statements
    public String toString() {
        return super.toString() + "Brand: " + this.brand + "\n" + "Sport: " + this.sportkind + "\n" +
                "Quantity: " + this.quantity + "\n" + stock() + "\n";
    }
//calculating price
    public double calculateTotalPrice () {
        return ((getPrice())*(getTax() + 100)/100)*quantity;
    }
}
