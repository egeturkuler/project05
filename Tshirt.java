package project01;

public class Tshirt extends Cloth {

    // taking all constructors from the class above with super function
    public Tshirt(String name, double tax, double price, String refund, String size, String brand, double quantity){
        super(name, tax, price, refund, size, brand, quantity);
    }
    //calculating price
    public double calculateTotalPrice () {
        return ((getPrice())*(getTax() + 100)/100)*getQuantity();
    }

    //deciding whether or not the product is shrinkable, also method override
    public void shrink(){
        System.out.println("This clothing won't shrink when washed at high temperature.");
    }



}
