package project01;

public class Videogame extends Product {
    private String console; //constructors are private because they will be used only here
    private String gamename;

    public Videogame(String name, double tax, double price, String refund, String console, String gamename) {
        super(name, tax, price, refund);
        this.gamename = gamename;
        this.console = console;
    }
    //does the game have an award or not, decided by method overload

    public String award(String a) {
        return "This video game has no award.";
    }

    public String award(int x) {
        return "This video game has won 'Game of The Year'.";
    }
    //printing statements

    public String toString() {
        return super.toString() + "Game: " + this.gamename + "\n" + "Platform: " + this.console + "\n" + award(5) +
                "\n";
    }
    //calculating price

    public double calculateTotalPrice () {
        return ((getPrice())*(getTax() + 100)/100);
    }
}
